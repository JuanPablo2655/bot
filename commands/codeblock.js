"use strict";

// Requirements
const app = require("./../app.js");
const discord = app.discord;

// Details about the command.
const properties = {
	command: "codeblock",
	description: "Displays code block usage.",
	visible: true,
	arguments: []
}

// Runs when the command is triggered.
function run(message) {
	const embed = new discord.RichEmbed();
	embed.setTitle("Codeblock Tutorial");
	embed.setDescription("Please use code blocks when sending code.");
	embed.attachFiles(["./assets/codeblocks.png"]);
	embed.setImage("attachment://codeblocks.png");
	
	message.channel.send({embed});
	
}


exports.properties = properties;
exports.run = run;